var express = require("express");
//console.log("Contenido de la variable express",express);
var bodyparser = require("body-parser");
var app = express();

var cita_routers = require('./src/routers/cita_routers');
var persona_routers = require('./src/routers/persona_routers');
var tratamiento_routers = require('./src/routers/tratamiento_routers');
/* methodOverride = require("method-override"); */
/* mongoose = require ('mongoose'); */

/* console.log("Contenido de la variable bodyparser",bodyparser);
console.log("Contenido de la variable methodOverride",methodOverride);
console.log("Contenido de la variable mongoose",mongoose); */

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

//configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-with, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

/* app.use(require('./routers/routers')); */

//rutas
app.use('/',cita_routers);
app.use('/',persona_routers);
app.use('/',tratamiento_routers);
//HTML SIEMPRE TIENE UNA PREGUNTA Y UNA RESPUESTA (REQ,RES)
//exportar
module.exports = app;