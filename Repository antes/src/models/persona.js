var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PersonaSchema = Schema({
    documento: String,
    nombre: String,
    especialidad: String,
    rol: String
});

const Persona = mongoose.model('persona', PersonaSchema);
module.exports = Persona;
