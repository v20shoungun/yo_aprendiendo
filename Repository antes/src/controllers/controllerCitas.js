
//const { default: mongoose } = require("mongoose");
const Cita = require("../models/Cita");

function prueba(req, res){
    res.status(200).send({
        message: 'Haciendo una prueba'
    });
}

function saveCita(req, res){
    var myCita = new Cita(req.body);
    myCita.save((err, result) => {
        res.status(200).send({messages:result});
    });
}

function buscarData(req, res){
        var idCita=req.params.id;
        Cita.findById(idCita).exec((err,result) => {
            if(err){
                res.status(500).send({message:'pailas'})
            }else{
                if(!result){
                    res.status(404).send({message:'no se encuentra'})
                }else{
                    res.status(200).send({result});
                }
        
            }
        });

}

function listarAllData (req, res){
    var idCita = req.params.idb;
    if(!idCita){
        var result = Cita.find({}).sort('type');
    }else{
        var result =Cita.find({cita:idCita}).sort('type');
    }

    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'pailas'})
        }else{
            if(!result){
                res.status(404).send({message:'no se encuentra'})
            }else{
                res.status(200).send({result});
            }
    
        }
    });
}

function deleteCita(req, res){
    var idCita=req.params.id;
    Cita.findByIdAndDelete(idCita).exec((err,result) =>{
        if(err){
            res.status(500).send({message:'pailas'})
        }else{
            if(!result){
                res.status(404).send({message:'no se encuentra'})
            }else{
                res.status(200).send({result});
            }
    
        }
    });

}

function upDateCita(req, res){
    /* var id = mongoose.Types.ObjectId(req.query.productId); */
    var id = req.params.id;
    Cita.findByIdAndUpdate({_id:id},req.body,{new:true},
        function(err,Cita){
        if(err){
            res.send(err);
        }
        res.json(Cita);
    });
}



module.exports = {
    prueba, 
    saveCita, 
    listarAllData,
    buscarData, 
    deleteCita,
    upDateCita
}