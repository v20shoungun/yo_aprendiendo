const {Router} = require('express');
var controllerCita = require('../controllers/controllerCitas')
const router = Router();
router.get('/prueba', controllerCita.prueba); 
router.post('/creacion', controllerCita.saveCita);
router.get('/buscarTodos/:idb?', controllerCita.listarAllData);
router.get('/buscarId/:id', controllerCita.buscarData);
router.put('/actualizacion/:id',controllerCita.upDateCita);
router.delete('/eliminar/:id', controllerCita.deleteCita);
/* export default router; */
module.exports = router;