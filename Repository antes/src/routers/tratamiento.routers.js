const {Router} = require('express');
var controllerTratamiento = require('../controllers/controllerTratamiento')
const router = Router();

router.get('/prueba', controllerTratamiento.prueba);
router.post('/creacion', controllerTratamiento.saveTratamiento);
router.get('/buscarTodos/:idb?', controllerTratamiento.listarAllData);
router.get('/buscarId/:id',controllerTratamiento.buscarData);
router.put('/actualizacion/:id',controllerTratamiento.upDateTratamiento);
router.delete('/eliminar/:id',controllerTratamiento.deletetratamiento);


module.exports =  router; 

