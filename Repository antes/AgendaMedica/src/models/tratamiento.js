var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TratamientoSchema = Schema({
    
    id: String,
    nombre: String,
    tipo: String,
    indicaciones: String

});

const Tratamiento = mongoose.model('tratamiento', TratamientoSchema);
module.exports = Tratamiento;