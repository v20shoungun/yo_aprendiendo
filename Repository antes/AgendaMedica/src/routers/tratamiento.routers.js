const tratamientoController = require("../controllers/controllerTratamiento");
const { Router } = require("express");
const router = Router();

router.get('/buscarTratamiento/:id',tratamientoController.buscarDataId);
router.get('/buscarResultados/:idb?', tratamientoController.listarAllData);
router.post('/crearTratamiento', tratamientoController.saveTratamiento);
router.delete('/remove/:id',tratamientoController.deleteTratamiento);
router.put('/cambiar/:id',tratamientoController.updateTratamiento);


module.exports =  router;